#!/usr/bin/bash

mode=$1
#step 1 : compile the core module
: ' cd $SPARK_HOME
./build/mvn -pl :spark-core_2.11 clean package -DskipTests -T 4

#step 2 : copy the jars from target core to assembly folder of spark

dest=$SPARK_HOME'/assembly/target/scala-2.11/jars/spark-core_2.11-2.3.0.jar'
source=$SPARK_HOME'/core/target/spark-core_2.11-2.3.0.jar'
mv $source $dest

'
#step 2.1 : Check if first arg is standalone.if yes spawn the master and two workers
if [ "$1" = "standalone" ]
then
	$SPARK_HOME/sbin/start-master.sh
	$SPARK_HOME/sbin/start-slave.sh spark://jimsppc:7077
fi

#step 3: Submit the job using spark-submit
$SPARK_HOME/bin/spark-submit --class org.test.spark.Workflow /home/dimitris/Desktop/MySpark.jar

#step 3.1: Close workers and master if standalone mode
if [ "$1" = "standalone" ]
then
	$SPARK_HOME/sbin/stop-master.sh
	$SPARK_HOME/sbin/stop-slave.sh
fi
